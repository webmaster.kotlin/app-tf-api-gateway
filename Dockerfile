FROM node:latest
WORKDIR /usr/src/app

COPY . .
COPY package.json ./
COPY yarn.lock ./
RUN yarn install


EXPOSE 8080
CMD [ "yarn", "run", "start" ]