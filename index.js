const { v4: uuidv4 } = require('uuid');
const { exec }       = require("child_process");
const bodyParser     = require('body-parser');
const express        = require('express');
const EXPRESS_PORT   = process.env.EXPRESS_PORT || 8080;

const app = express();
const port = EXPRESS_PORT;
app.use(bodyParser.json());

////
app.all('/', (req, res) => {
  res.status(204).json({
    status: "success",
    message: "Health Checks",
    data: uuidv4(),
    action: 'checked'
  });
});
app.all('/health', (req, res) => {
  res.status(204).json({
    status: "success",
    message: "Health Checks",
    data: uuidv4(),
    action: 'checked'
  });
});

///

app.all('/api/customers', (req, res) => {
  res.status(200).json({
    status: "success",
    message: "Customers endpoint is working",
    data: uuidv4(),
    action: req.method
  });
});

app.all('/api/payments', (req, res) => {
  res.status(200).json({
    status: "success",
    message: "Payments endpoint is working",
    data: uuidv4(),
    action: req.method
  });
});

app.all('/api/payments/information/:id', (req, res) => {
  res.status(200).json({
    status: "success",
    message: "Payments information endpoint is working",
    data: {"transactionId":req.params.id},
    action: req.method
  });
});

app.all('/api/qrcode', (req, res) => {
  res.status(200).json({
    status: "success",
    message: "QrCode endpoint is working",
    data: Buffer.from(uuidv4()).toString('base64'),
    action: req.method
  });
});


app.listen(port, () => {
  console.log(`App listening at http://localhost:${port}`);
});